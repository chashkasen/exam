#include <iostream>
#include <inc/wkey.hpp>

namespace km
{
	Key::Key(int x, int y, float width, float length)
	{
		m_x = x;
		m_y = y;
		m_width = width;
		m_length = length;
		m_key = new sf::RectangleShape(sf::Vector2f(m_width, m_length));
		m_key->setOrigin(m_width / 2, m_length / 2);
		m_key->setPosition(m_x, m_y);
		m_key->setFillColor(sf::Color::White);

	}

	sf::RectangleShape* Key::Get() { return m_key; }

	Key::~Key()
	{
		delete m_key;
	}
}