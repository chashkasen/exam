#include <iostream>
#include <inc/bkey.hpp>

namespace km
{
	BlackKey::BlackKey(int x, int y, float width, float length)
	{
		m_x = x;
		m_y = y;
		m_width = width;
		m_length = length;
		m_keyb = new sf::RectangleShape(sf::Vector2f(m_width, m_length));
		m_keyb->setOrigin(m_width / 2, m_length / 2);
		m_keyb->setPosition(m_x, m_y);
		m_keyb->setFillColor(sf::Color::Black);
	}

	BlackKey::~BlackKey()
	{
		delete m_keyb;
	}

	sf::RectangleShape* BlackKey::Get() { return m_keyb; }
}