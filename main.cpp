#include <SFML/Graphics.hpp>
#include <iostream>
#include <thread>
#include <chrono>
#include <inc/wkey.hpp>
#include <inc/bkey.hpp>
#include <vector>
#include <SFML/Audio.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
using namespace std::chrono_literals;

int main()
{

    sf::Music note1;
    if (!note1.openFromFile("sound/do.ogg"))
        return -1; 

    sf::Music note2;
    if (!note2.openFromFile("sound/re.ogg"))
        return -1;

    sf::Music note3;
    if (!note3.openFromFile("sound/mi.ogg"))
        return -1;

    sf::Music note4;
    if (!note4.openFromFile("sound/fa.ogg"))
        return -1;

    sf::Music note5;
    if (!note5.openFromFile("sound/sol.ogg"))
        return -1;

    sf::Music note6;
    if (!note6.openFromFile("sound/la.ogg"))
        return -1;

    sf::Music note7;
    if (!note7.openFromFile("sound/si.ogg"))
        return -1;

    sf::Music note8;
    if (!note8.openFromFile("sound/do2.ogg"))
        return -1;
   

    sf::Texture texture;
    if (!texture.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR: not found back.jpg" << std::endl;
        return -1;
    }

    const int L = 1024; const int H = 800;

	sf::RenderWindow window(sf::VideoMode(L, H), "Piano Game");

    sf::Image icon;
    if (!icon.loadFromFile("img/piano.png"))
    {
        return -1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

    sf::RectangleShape base(sf::Vector2f(905, 410));
    base.setFillColor(sf::Color::Color(102,0,0,250));
    base.setPosition(L/2, H-230);
    base.setOrigin(452, 200);

    // 
    int x = 110, a = 382;
    std::vector<km::Key*> keys;
    keys.push_back(new km::Key(L / 2 - a, H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-x), H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-2*x), H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-3*x), H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-4*x), H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-5*x), H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-6*x), H - 220, 100, 400));
    keys.push_back(new km::Key(L / 2 - (a-7*x), H - 220, 100, 400));

    // 
    a = 327;
    std::vector<km::BlackKey*> bkeys;
    bkeys.push_back(new km::BlackKey(L / 2 - a, H - 320, 50, 200));
    bkeys.push_back(new km::BlackKey(L / 2 - (a-x), H - 320, 50, 200));
    bkeys.push_back(new km::BlackKey(L / 2 - (a-3*x), H - 320, 50, 200));
    bkeys.push_back(new km::BlackKey(L / 2 - (a-4*x), H - 320, 50, 200));
    bkeys.push_back(new km::BlackKey(L / 2 - (a-5*x), H - 320, 50, 200));
    bkeys.push_back(new km::BlackKey(L / 2 + 426, H - 320, 25, 200));  

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
       
            for (int i = 0; i < 2; i++)
            {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
                    note1.play();  

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
                    note2.play();

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
                    note3.play();

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
                    note4.play();

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
                    note5.play();
                
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
                    note6.play();

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::J))
                    note7.play();

                if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
                    note8.play();
            }

        window.clear();

        window.draw(base);

        for (const auto& key : keys)
            window.draw(*key->Get());

        for (const auto& bkey : bkeys)
            window.draw(*bkey->Get());

        window.display();

        std::this_thread::sleep_for(40ms);

    }

	return 0;
}
