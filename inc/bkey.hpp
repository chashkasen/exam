#pragma once
#include <SFML/Graphics.hpp>

namespace km
{
	class BlackKey
	{
	public:
		BlackKey(int x, int y, float width, float length);


		~BlackKey();


		sf::RectangleShape* Get();

	private:
		int m_x;
		int m_y;
		float m_width;
		float m_length;
		sf::RectangleShape* m_keyb;
	};
}